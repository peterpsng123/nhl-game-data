# NHL Game Data

Using the data in https://www.kaggle.com/martinellis/nhl-game-data
Performing data exploration, data cleaning, data quality checking, data transformation, data analysis.